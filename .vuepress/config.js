const container = require("markdown-it-container");

function createContainer(type, defaultTitle) {
  return [
    container,
    type,
    {
      render(tokens, idx) {
        const token = tokens[idx];
        const info = token.info
          .trim()
          .slice(type.length)
          .trim();
        const title = info || defaultTitle;
        if (token.nesting === 1) {
          return `<div class="${type} custom-block"><p class="custom-block-title">${title}</p>\n`;
        } else {
          return `</div>\n`;
        }
      },
    },
  ];
}

module.exports = {
  title: "oop2",
  description: "Objektorientierte Programmierung 2",
  configureWebpack: (config, isServer) => {
    return {
      node: {
        global: !isServer,
      },
      module: {
        rules: [
          {
            test: /\.txt$/i,
            use: "raw-loader",
          },
        ],
      },
    };
  },
  plugins: [
    [
      "container",
      {
        type: "lecture",
        defaultTitle: "Unterricht",
        before: (info) => `<table class="lecture"><tr><td>${info}</td><td>`,
        after: "</td></tr></table>",
      },
    ],
    [
      "@vuepress/pwa",
      {
        serviceWorker: true,
        updatePopup: {
          message: "Update der Seite vefügbar",
          buttonText: "Aktualisieren",
        },
      },
    ],
    [
      "@vuepress/last-updated",
      {
        transformer: (timestamp, lang) => {
          // Don't forget to install moment yourself
          const moment = require("moment");
          moment.locale(lang);
          return moment(timestamp).format("DD.MM.YYYY HH:mm");
        },
      },
    ],
    [require("@nexuscomputing/vuepress-classroom"), {}],
  ],
  themeConfig: {
    sidebar: [
      {
        title: "Home",
        collapsable: false,
        children: ["/", "drehbuch/"],
      },
      {
        title: "Slides",
        collapsable: false,
        children: [
          "/slides/01 Git/",
          "/slides/02 Recap/",
          "/slides/03 Swing GUI/",
          "/slides/04 Model View Controller/",
          "/slides/05 Abstraction/",
          "/slides/06 Strings/",
        ],
        sidebarDepth: 2,
      },
      {
        title: "Versuche",
        children: [
          "/versuche/00 Setup/",
          "/versuche/01 Git Started/",
          "/versuche/02 Intro/",
          "/versuche/03 GridBagLayout/",
          "/versuche/03 MVC/",
        ],
        collapsable: false,
        sidebarDepth: 2,
      },
    ],
    sidebarDepth: 2,
    lastUpdated: "Letztes Update", // string | boolean
    // default value is true. Set it to false to hide next page links on all pages
    nextLinks: false,
    // default value is true. Set it to false to hide prev page links on all pages
    prevLinks: false,
    // displayAllHeaders: true
  },
  dest: "public",
  base: "/oop2/",
  markdown: {
    lineNumbers: true,
    extendMarkdown: (md) => {
      md.use(require("markdown-it-imsize"));
      md.use(require("markdown-it-footnote"));
      md.use(...createContainer("front", ""));
    },
  },
  head: [
    ["link", { rel: "icon", href: "/images/icons/icon-96x96.png" }],
    ["link", { rel: "manifest", href: "/manifest.json" }],
    [
      "link",
      {
        rel: "mask-icon",
        href: "/icons/logo.svg",
        color: "#3eaf7c",
      },
    ],
    [
      "link",
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/icon?family=Material+Icons",
      },
    ],
    [
      "link",
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/icon?family=Material+Icons",
      },
    ],
    [
      "link",
      {
        rel: "stylesheet",
        href:
          "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css",
      },
    ],
    [
      "link",
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i&display=swap",
      },
    ],
    [
      "link",
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css?family=Oswald",
      },
    ],
    [
      "link",
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css?family=Source+Code+Pro:200,300,400,500,600,700,900&display=swap",
      },
    ],
    [
      "link",
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css?family=Merriweather:300,300i,400,400i,700,700i,900,900i|Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&display=swap",
      },
    ],
  ],
};
