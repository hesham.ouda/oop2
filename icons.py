#!/usr/bin/python3

import os

SIZES = [72, 96, 128, 144, 152, 192, 384, 512]
OUTPUT_NAME = "icon-%S-%S.png"
DIR = "./.vuepress/public/images/icons"


def commandline(source, destprefix, size):
    return f"inkscape -C -y 0 -e {destprefix}-{size}x{size}.png -w {size} {source}"


def batch_resize_icons(source):
    print("exporting")
    for size in SIZES:
        cmd = commandline(source, f"{DIR}/icon", size)
        print(f"running: {cmd}")
        res = os.popen(cmd).read()
        print(res)


if __name__ == "__main__":
    batch_resize_icons("logo.svg")
    pass
