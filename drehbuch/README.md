---
title: "Drehbuch"
author: [Manuel Di Cerbo]
subject: "OOP2"
keywords: [Drehbuch]
prev: false
next: false
---

# Drehbuch: Modul OOP2 (Objektorientierte Programmierung 2)

### Leitidee

Software ist heute nicht mehr wegzudenken. Smartphones, Tablets, Notebooks und Embedded Systems begleiten uns Tag für Tag und dienen uns als Werkzeuge für unsere täglichen Aufgaben.

Wie funktioniert Software? Wie kann ich mir selber Tools und Werkzeuge erarbeiten? Das sind zwei Kernfragen, welche im Modul OOP2 adressiert werden. Neben einem neuen Kanal für Kreativität bieten Kenntnisse in Programmierung auch praktische Hilfe um vor allem repetitive Probleme zu Lösen.

Dieses Modul vermittelt den Umgang mit der Programmiersprache Java. Einführend hilft OOP2 das erlernen einer Programmiersprache und die Strukturierung und Modellierung von Software via Objektorientierung. Das ultimative Ziel: Selber in der Lage zu sein Probleme zu analysieren, abstrahieren und via Software zu lösen.

Bemerkung: Die Lernziele sind in der Modulbeschreibung unter https://www.fhnw.ch/de/studium/technik/elektro-und-informationstechnik zu finden und werden deshalb hier nicht wiederholt.

## Unterrichtsorganisation

### Lern- und Arbeitsformen

Die Hälfte des Unterrichts wird für die Erarbeitung der theoretischen Grundlagen eingesetzt. Die andere Hälfte steht den Teilnehmern in der Form eines begleiteten Selbststudiums zur Verfügung.

Hausübungen und unbegleitetes Selbststudium bilden den restlichen Teil des Moduls.

#### Distance Learning  <span class="orange-chip">Spezialregelung FS21</span>

Begleitend zum Frontalunterricht wird die Stoffvermittlung zum Teil auch als E-Learning in Form von Lernvideos (Podcasts) und Begleiteten Übungen via Online-Meetings (WebEx, Zoom, etc.) stattfinden.

### Zeitaufwand für Studierende (summarisch)

| Form                                                   | Aufwand |
| ------------------------------------------------------ | ------- |
| Frontalunterricht und Distance Learning                | ca. 15h |
| Begleitete Übungen                                     | ca. 30h |
| Unbegleitetes Selbststudium ausserhalb Unterrichtszeit | ca. 45h |
| Gesamtaufwand                                          | ca. 90h |

### Anwesenheit

**Anwesenheitspflicht gilt während der Prüfungen / Tests / Klasuren**. Unentschuldigte Absenzen während einer Klausur werden gemäss Reglement der Hochschule (resp. Fakultät) handgehabt.

Es besteht **keine** generelle Anwesenheitspflicht. Der Unterrichtsstoff stellt sich aus Lehrmittel und Übungen bzw. Versuchen zusammen, welche der Teilnehmende selbständig erarbeiten können.

## Leistungsbeurteilung (KISA)

Die Modulnote besteht aus zur Hälfte aus einer schriftlichen MSP und zur anderen hälfte aus der Erfahrungsnote, welche sich aus folgenden Gewichtungen der Leistungen zusammensetzt.

|        | Gewicht |
| ------ | ------- |
| Quiz 1 | 1/2\*   |
| Quiz 2 | 1/2\*   |
| Quiz 3 | 1/2\*   |

(\*) Bei der Absolvierung aller drei Quiz wird die schlechteste Note gestrichen.

- Alle Quiznoten werden auf 1/10 gerundet.
- Die resultierende Erfahrungsnote wird auf 1/10 gerundet.
- Die Note der Modulschlussprüfung wird auf 1/10 gerundet.
- Die Modulnote wird auf 1/10 gerundet.

Ein Modul OOP2 gilt als bestanden, wenn die Modulnote, gerundet auf eine Note, ≥ 4.0 ist.

### Arbeitsmittel

Der Unterricht basiert auf: Douglas Bell, Mike Parr: „Java for Students“, Prentice Hall, Februar 2006, ISBN: 0131735799.

Der Inhalt wird im Unterricht anhand von Slides erläutert.

Als Nachschlagewerk wird Java ist auch eine Insel, 10., aktualisierte Auflage, Galileo Computing ISBN 978-3-8362-1506-0 empfohlen HTML – Version wird abgegeben). Als Entwicklungsumgebung kommt Eclipse zum Einsatz.

### Abweichungen

Abweichungen und Änderungen an diesem «Drehbuch» sind, wo sinnvoll und nötig, jederzeit vorbehalten.

## Termine

<LectureTable/>
