---
title: "OOP1 Recap"
author: [Manuel Di Cerbo]
subject: "oop2"
sidebarDepth: 2
---

<SlideDeck>

<Slide center>

## Zusammenfassung OOP1

Objektorientierte Programmierung 2

</Slide>

<Slide>

## Primitive Datentypen

```java
int i = 0;
long l = 0l;
float f = 0.0f;
double d = 0.0;
```

</Slide>

<Slide>

## Zuweisungen

```java
double x = 7.0;
double y = quadrat(x);
```

</Slide>

<Slide style="font-size: 80%">

## Conditionals / Verzweigungen

```java
if(i > 0) {
    System.out.println("i ist grösser als 0: " + i);
}
```

</Slide>

<Slide>

## Schleifen / Loops

```java
while(x > 0){
    x--;
}

for(int i = 0; i < 10; i++) {
    System.out.println("Counter: " + (i +1)));
}
```

</Slide>

<Slide>

## Methoden

```java
void hello() {
    System.out.println("hello!");
}
```

```java
double quadrat(double n) {
    return n * n;
}
```

</Slide>

<Slide>

## Überladen

```java
public static double quadrat(double x) {
    System.out.println("quadrat(double x)");
    return x * x;
}

public static float quadrat(float x) {
    System.out.println("quadrat(float x)");
    return x * x;
}

```

</Slide>

<Slide style="font-size: 65%">

## Klassen

```java
class Ball {
    int x, y, size;
    Color color;

    public Ball(int size, Color color) {
        this.size = size;
        this.color = color;
    }

    public void paint(Graphics g) {
        g.setColor(color);
        g.fillOval(x, y, size);
    }
}

```

</Slide>


<Slide style="font-size: 65%">

## Vererbung

```java
class SquareBall extends Ball {

    public SquareBall(int size, Color color) {
        super(size, color);
    }

    public void paint(Graphics g) {
        g.setColor(color);
        g.fillRect(x, y, size, size);
    }
}

```

</Slide>

<Slide style="font-size: 65%">

## Instanzieren

```java

private Ball roterBall = new Ball(10, Color.RED);
private SquareBall blauesQuadrat = new SquareBall(10, Color.BLUE);


// Zugriff auf Attribut

System.out.println("Roter Ball x: " + roterBall.x);
```

</Slide>


<Slide center>

## Klassendiagramme

<img src="./klassendiagramm.png" style="width: 80%" />

</Slide>


<Slide style="font-size: 75%">

## Ereignisorientierte Programmierung

```java
startButton.addActionListener(new ActionListener() {
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Starting!");
    }
});
```

</Slide>

</SlideDeck>
