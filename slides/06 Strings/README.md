---
title: "Strings"
author: [Manuel Di Cerbo]
subject: "oop2"
sidebarDepth: 2
---

<SlideDeck>

<Slide center>

## Strings

Objektorienteirte Programmierung 2

</Slide>

<Slide style="font-size: 65%">

## Vergleichen

```java
// SO NICHT
String greetings = "hello";

if(greetings == "hello") {
    System.out.println("it's a match");
}


// sondern so!
f(greetings.equals("hello")) {
    System.out.println("it's a match");
}
```

</Slide>

<Slide style="font-size: 75%">

## equalsIgnoreCase

```java
String greetings = "hello";


if(greetings.equalsIgnoreCase("Hello")){
    System.out.println("it's a match");
}
```

</Slide>


<Slide style="font-size: 75%">

## Formatieren von Strings und Zahlen

```java
int count = 7;

String message = String.format("count is %05d", count);
System.out.println(message);
// count is 00007
```

</Slide>

<Slide style="font-size: 75%">

## Formatieren von `double`

```java
double pi = Math.PI;
String message = String.format("pi is about %.4f", pi);
System.out.println(message);
// pi is about 3.1416
```

</Slide>

<Slide>

## Sonderzeichen in Strings

| Sequence |                 |
| -------- | --------------- |
| `\n`     | Newline         |
| `\\`     | `\`             |
| `\r`     | Carriage Return |
| `\t`     | Tabulator       |

</Slide>

<Slide style="font-size: 75%">

## String Methoden

| Methode      |                                               |
| ------------ | --------------------------------------------- |
| `equals`     | Inhaltlicher Vergleich                        |
| `replace`    | Zeichen ersetzen                              |
| `trim`       | White Space am Anfang und Ende entfernen      |
| `length`     | Länge der Zeichenkette                        |
| `substring`  | Gibt eine Teilkette zurück                    |
| `charAt`     | Zeichen an der Stelle des Index               |
| `indexOf`    | Zeichenkette in der Kette suchen              |
| `endsWith`   | Ob die Zeichenkette mit einer anderen endet   |
| `startsWith` | Ob die Zeichenkette mit einer anderen startet |


</Slide>

</SlideDeck>