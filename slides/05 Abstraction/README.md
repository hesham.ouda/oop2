---
title: "Abstraction"
author: [Manuel Di Cerbo]
subject: "oop2"
sidebarDepth: 2
---

<SlideDeck>

<Slide center>

## Abstraction

Objektorientierte Programmierung 2

</Slide>


<Slide>

## Werkzeuge der Abstraktion

- Klasse
- Interface
- Vererbung
- Abstrakte Klasse
- Generics

</Slide>

<Slide center>

## Warum abstrahieren?

- Code wiederverwenden
- Code testbar machen
- Code mit weniger Fehlern erstellen
- weniger Copy-Paste



</Slide>


<Slide>

## Klasse

- Eine Klasse ist ein Datentyp, welcher Attribute und Methoden haben kann.
- `static` beschreibt Attribute und Methoden, welche ohne Instanz verwendet werden können (*stateless*).
- Attribute und Methoden hängen immer an einer Instanz einer Klasse und sind darum *stateful*.

</Slide>

<Slide>

## Instrumente einer Klasse

- `extends` heisst die Klasse erbt von einer anderen Klasse
- `implements` heisst die Klasse implementiert ein Interface

</Slide>


<Slide center>

## Erben

Eine Klasse, welche von einer Basisklasse erbt, beinhaltet alle Attribute und Methoden der Basisklasse.

Eine Klasse kann immer nur ***von genau einer*** anderen Klasse erben.

</Slide>

<Slide style="font-size: 65%">

## Erben: Beispiel

```java
public class Character {

	private String name = "";

	public Character(String name) {
		this.name = name;
	}

	void introduce() {
		System.out.println("Hi, I am " + name);
	}
}

class Mario extends Character {

	public Mario(String name) {
		super("Mario");
	}
}
```

</Slide>


<Slide center>

## `final class`

Eine Klasse, welche als `final` deklariert wurde, kann nicht weitervererbt werden.

</Slide>

<Slide center>

## Interfaces

Ein Interface beschreibt mindestens eine Methode, welche von der Klasse implementiert werden ***muss***.

Eine Klasse, welche ein Interface implementiert, kann immer in das Interface gecastet werden.

</Slide>

<Slide center>

## Interfaces zur Abstraktion

Interfaces sind absolut zentral bei der Abstraktion. Sie erlauben das saubere trennen von Funktionalität einer Klasse.

Interfaces dienen als Hauptinstrument um hohe Testbarkeit und Modularität zu erreichen.

</Slide>

<Slide>

## Namen von Interfaces

Oft werden Verben im Partizip verwendet. `Clickable`, `Drawable`, `Observable`. Der Name beschreibt oft, was das Interface "machen kann".

</Slide>

<Slide style="font-size: 65%">

## Interfaces Beispiel

```java
interface Presentable {
	void present();
}

public class Character implements Presentable {

	private String name;

	@Override
	public void present() {
		System.out.println("Hey, I am " + name);
	}
}
```

</Slide>

<Slide style="font-size: 65%">

## Anonyme Objekte von Interfaces instanzieren

```java
interface Presentable {
	void present();
}

public class Main {

	public static void main(String[] args) {
		Presentable mario = new Presentable() {
			@Override
			public void present() {
				System.out.println("Hi, it's a me, Mario");
			}
		};
	}
}
```

</Slide>

<Slide center>

## Abstrakte Klassen

Interfaces versprechen Methoden. Interfaces können *nie* Attribute vorschreiben. Abstrakte Klassen erlauben genau das.

Abstrakte Klassen können nicht instanziert werden.

</Slide>

<Slide>

## Abstrakte Klasse wie ein Interface verwenden

Wie beim Interface können abstrakte Klassen Methoden als `abstract` deklarieren. Das heisst, die Methode muss von der erbenden Klasse implementiert werden. Diese Funktionalität widerspiegelt 1:1, was mit Interfaces möglich ist.

</Slide>

<Slide style="font-size: 50%">

## Beispiel abstract class

```java

abstract class Character {
	String name;

	public Character(String name) {
		this.name = name;
	}

	abstract void present();
}

class Mario extends Character {

	public Mario(String name) {
		super("Mario");
	}

	@Override
	public void present() {
		System.out.println("Hi, it'sa me, " + name);
	}
}
```

</Slide>

</SlideDeck>