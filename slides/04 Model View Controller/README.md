---
title: "Model View Controller"
author: [Manuel Di Cerbo]
subject: "oop2"
sidebarDepth: 2
---

<SlideDeck>

<Slide center>

# Model - View - Controller

Objektorientierte Programmierung 2

</Slide>

<Slide center>

## MVC Intro

MVC ist ein Pattern, welches bei GUI eingesetzt wird. Ziel ist es, Applikationen zu strukturieren, um auch bei komplexeren Zusammenhängen den Überblick zu behalten.

</Slide>

<Slide center>

## View

Die View ist für das Darstellen des GUI verantwortlich.

- Layout handeln
- Zustand des Models für den User darstellen
- Events an Controller weitergeben

Die View ändert das Model **_nie_**!

</Slide>

<Slide center>

## Controller

Der Controller ist das Zwischenstück zwischen Model und View. Er handelt Events und den Zustand der Applikation. Auch pflegt er das Model und kann das Model ändern.

</Slide>

<Slide>

## Model

Das Model ist der Datenuntersatz für die View und den Controller. Es ist vom Rest der Applikation unabhängig und sollte immer testbar sein.

- Das Model weiss nichts von der View
- Das Model weiss nichts von AWT / Swing
- Das Model weiss nichts vom Controller

</Slide>

<Slide>

## Observable Model

Ein Model, welches Obervable ist, informiert Observer über Änderungen.

</Slide>

<Slide center>

## Beispiel: Roll the Dice

![Dice](./dice.png)

</Slide>

<Slide>

## Model

```java
public class Model {

	public int number = 0;

	public void roll() {
		number = (int) (Math.random() * 6) + 1;
	}
}
```

</Slide>

<Slide style="font-size: 65%">

## Controller

```java

public class Controller {

	private Model model;
	private MainPanel view;

	public Controller(Model model) {
		this.model = model;
	}

	public void install(MainPanel view) {
		this.view = view;
	}

	public void roll() {
		model.roll();
		view.update(model);
	}

}
```

</Slide>

<Slide style="font-size: 35%">

## View

```java
public class MainPanel extends JPanel {

	private Controller controller;

	public MainPanel(Controller controller) {
		this.controller = controller;
	}

	JLabel resultat = new JLabel(" - ");
	JButton button = new JButton("ROLL");

	private final Insets insets = new Insets(15, 15, 15, 15);

	public void init() {
		setLayout(new GridBagLayout());

		resultat.setFont(new Font("Monospaced", Font.PLAIN, 40));
		resultat.setBackground(Color.DARK_GRAY);
		resultat.setForeground(Color.WHITE);
		resultat.setOpaque(true);

		add(resultat, new GridBagConstraints(0, 0, 1, 1, 0, 1, GridBagConstraints.CENTER, GridBagConstraints.NONE,
				insets, 0, 0));

		add(button, new GridBagConstraints(0, 1, 1, 1, 0, 1, GridBagConstraints.CENTER, GridBagConstraints.NONE, insets,
				0, 0));

		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.roll();
			}
		});
		invalidate();
	}

	public void update(Model model) {
		resultat.setText(" " + model.number + " ");
	}
}
```

</Slide>

<Slide style="font-size: 75%">

## App

```java
// ... main

final Model model = new Model();
final Controller controller = new Controller(model);
final MainPanel view = new MainPanel(controller);
controller.install(view);

JFrame frame = new JFrame("Roll the Dice");
frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

view.setPreferredSize(new Dimension(250, 250));
view.setDoubleBuffered(true);
frame.add(view);

// ...
```

</Slide>

</SlideDeck>
