---
title: "Swing GUI"
author: [Manuel Di Cerbo]
subject: "oop2"
sidebarDepth: 2
---

<SlideDeck>

<Slide center>

## SWING Graphical User Interface

Objektorientierte Programmierung 2

</Slide>

<Slide center>

## Komponenten

<img src="./gallery.png" style="width: 80%" />

[A Visual Guide to Swing Components](https://web.mit.edu/6.005/www/sp14/psets/ps4/java-6-tutorial/components.html)

</Slide>

<Slide>

## Layout Manager

### Absolute Layout

```java
public void init() {
    setLayout(null);
}
```

Komponenten werden mit `.setBounds(x, y, w, h)` positioniert.

</Slide>

<Slide center>

## Nachteile von absoluten Layouts

Was passiert, wenn das Parent Panel nicht immer gleich gross ist?

</Slide>

<Slide>

## Layoutmanager

| Manager       | Verhalten                                         |
| ------------- | ------------------------------------------------- |
| FlowLayout    | Platziert Komponenten nebeneinander in Reihen     |
| BorderLayout  | Positioniert die Komponenten an den Fensterseiten |
| GridLayout    | Positioniert die Komponenten in einem Raster      |
| GridbagLayout | Flexible Platzierung                              |

[Layout Managers](https://docs.oracle.com/javase/tutorial/uiswing/layout/visual.html)

</Slide>

<Slide center>

## FlowLayout

<img src="./flow1.png" style="width: 300px" />
<img src="./flow2.png" style="width: 250px" />

</Slide>

<Slide center>

## BorderLayout

<img src="./border.png" style="width: 300px" />

</Slide>

<Slide center>

## GridLayout

<img src="./grid.png" style="width: 300px" />

</Slide>

<Slide center>

## GridBag

<img src="./gridbag.png" style="width: 300px" />

</Slide>

<Slide center>

## Aufbau

<img src="./nesting.png" style="width: 80%" />

</Slide>

<Slide>

## GridBagLayout

- Sehr flexibel
- Responsive (Panels können wachsen und schrumpfen)

</Slide>

<Slide style="font-size: 85%">

## GridBagLayout Code

```java
setLayout(new GridBagLayout());
add(bt1, new GridBagConstraints (gridx, gridy, gridwidth,
gridheight, weightx, weighty, anchor, fill, insets,
ipadx, ipady));
```

</Slide>

<Slide style="font-size: 65%">

## Constraints

| Param                 | Beschreibung                                                                                                                                                            |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| gridx, gridy          | Gibt die Zeile und Spalte der Zelle an.                                                                                                                                 |
| gridwidth, gridheight | Gibt die Breite und Höhe in Anzahl Zellen an.                                                                                                                           |
| weightx, weighty      | Gibt an, mit welchem Gewicht der horizontale und der vertikale Raum auf die Zellen verteilt wird.                                                                       |
| anchor                | Gibt an, wie die Komponente in der Zelle platziert wird.                                                                                                                |
| fill                  | Gibt an, wie sich die Komponente der Zelle anpassen soll.                                                                                                               |
| insets                | Gibt an, wieviel Raum in Pixeln, (oben, links, unten, rechts) ausserhalb Komponente hinzugefügt werden soll. Wird mittels new Insets(top, left, bottom, right) erzeugt. |
| ipadx, ipady          | Gibt an, wieviel Raum innerhalb der Komponenten hinzugefügt werden soll. Links und rechts erscheinen ipadx/2 Pixel, oben und unten erscheinen ipady/2 Pixel.            |

</Slide>

<Slide center>

## GridBagLayout

Mit etwas Übung können praktisch alle UI mit GridBagLayout in Code abgebildet werden.

</Slide>


<Slide>

## Beispiel 1 - Draft

<img src="./ex1-1.png" style="width: 80%" />


</Slide>


<Slide>

## Beispiel 1 - Draft 2

<img src="./ex1-2.png" style="width: 80%" />


</Slide>


<Slide>

## Beispiel 1 - Draft 3

<img src="./ex1-3.png" style="width: 80%" />


</Slide>


<Slide>

## Beispiel 1 - Draft 4

<img src="./ex1-4.png" style="width: 80%" />


</Slide>


<Slide  style="font-size: 35%">

## Beispiel 1 - Code

```java
public class MainPanel extends JPanel {

	JPanel panel = new JPanel();
	JButton button = new JButton("Start");

	private final Insets insets = new Insets(5, 5, 5, 5);

	JButton bt(String name) {
		return new JButton(name);
	}

	/**
	 * Run init code here (the view has valid dimensions at this point)
	 */
	public void init() {
		setLayout(new GridBagLayout());

		panel.setBackground(Color.DARK_GRAY);

		add(panel, new GridBagConstraints(0, 0, 1, 1, 1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets,
				0, 0));

		add(button, new GridBagConstraints(1, 0, 1, 1, 0, 0,
                GridBagConstraints.PAGE_END, GridBagConstraints.NONE,
				insets, 0, 0));

	}
}
```

</Slide>




<Slide>

## Beispiel 1 - Final

<img src="./ex1-5.png" style="width: 80%" />


</Slide>



<Slide>

## Beispiel 2 - Draft 1

<img src="./ex2-1.png" style="width: 80%" />


</Slide>


<Slide>

## Beispiel 2 - Draft 2

<img src="./ex2-2.png" style="width: 80%" />


</Slide>


<Slide>

## Beispiel 2 - Draft 3

<img src="./ex2-3.png" style="width: 80%" />


</Slide>





<Slide style="font-size: 35%">

## Beispiel 2 - Code

```java
public class MainPanel extends JPanel {

	JPanel panel = new JPanel();
	JTextField textField = new JTextField();
	JButton button = new JButton("Start");

	private final Insets insets = new Insets(5, 5, 5, 5);

	JButton bt(String name) {
		return new JButton(name);
	}

	/**
	 * Run init code here (the view has valid dimensions at this point)
	 */
	public void init() {
		setLayout(new GridBagLayout());

		panel.setBackground(Color.DARK_GRAY);

		add(panel, new GridBagConstraints(0, 0, 1, 1, 1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, insets,
				0, 0));

		add(button, new GridBagConstraints(1, 0, 1, 1, 0, 0,
                GridBagConstraints.PAGE_END, GridBagConstraints.NONE,
				insets, 0, 0));

		add(textField, new GridBagConstraints(0, 1, 2, 1, 0, 0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				insets, 0, 0));
	}
}

```

</Slide>


<Slide>

## Beispiel 2 - Final

<img src="./ex2-4.png" style="width: 80%" />


</Slide>


<Slide>

## Anchor in Komponente

<img src="./anchor-sketch.png" style="width: 80%" />


</Slide>


<Slide>

## Anchor

<img src="./anchor.png" style="width: 80%" />


</Slide>


</SlideDeck>
