---
title: "Git"
author: [Manuel Di Cerbo]
subject: "oop2"
sidebarDepth: 2
---

<SlideDeck>

<Slide center>

## Git

Intro zu Git

![Logo](./git.png)

</Slide>


<Slide>

## GIT CVS

Version Management Tool

- Geschrieben von Linux Torvalds
- Begründer / Erfinder von GIT
- Wird für den Kernel verwendet

</Slide>



<Slide dense>

## GIT vs. andere CVS

- GIT ist dezentral
  - ... hat keinen Single Point of Failure
- Alle git clones haben immer die ganze History
- Branches müssen nicht gemerged werden
  - ... führt zu viel weniger Konflikten
- Open Source unter GPL
- Kann Offline eingesetzt werde
  - um Checkpoints einzurichten
- Filebasiert
- schnell!!

</Slide>


<Slide>

## GIT vs GitHub vs GitLab

- GIT ist dezentral
  - ... hat keinen Single Point of Failure
- Alle git clones haben immer die ganze History
- Branches müssen nicht gemerged werden
  - ... führt zu viel weniger Konflikten
- Open Source unter GPL
- Kann Offline eingesetzt werde
  - um Checkpoints einzurichten

</Slide>


<Slide center>

## Repo erstellen

`git init`


</Slide>


<Slide center>

## Repo clonen (z.B. den Kernel)

`git clone https://github.com/torvalds/linux.git`

</Slide>


<Slide center>

## Branches anzeigen

`git branch -a`


*zeigt auch `remote` branches an*

</Slide>



<Slide center>

## Tags anzeigen

`git tag`

</Slide>


<Slide center>

## Auf Tag / Branch wechseln

`git checkout v5.5.7 -b local-v5.5.7`

</Slide>


<Slide>

## Git Checkout

- Wechselt auf einen anderen branch / tag
- `-b` erstellt einen ***lokalen*** neuen branch
- ... d.h. der lokale Branche ist nicht der gleiche Branche wie *upstream*!
- ... man kann so besser experimentieren

</Slide>


<Slide center>

## Änderungen von Upstream herunterladen

`git fetch`

</Slide>



<Slide center>

## Änderungen übernehmen (merge)

`git merge origin/master`

</Slide>


<Slide>

## Typischer workflow

```bash
git fetch
git merge origin/master

# kurz
git pull
```
</Slide>



<Slide center>

## Tracken, aufnehmen einer Änderung

`git commit -am "neues feature"`

</Slide>



<Slide center>

## Änderung für andere veröffentlichen

`git push origin master"`

</Slide>


<Slide>

## Konflikte

```
Auto-merging Main.java
CONFLICT (content): Merge conflict in Main.java
Automatic merge failed; fix conflicts and then commit the result.
```

- Muss von Hand aufgelöst werden.

</Slide>


<Slide>

## rebase vs. merge

- merge ist chronologisch
- rebase ist chronologisch, aber in einem branch
  - ... d.h. commits die logisch zusammengehören werden nicht im Zick-Zack in die History eingewebt.

</Slide>



<Slide>

## GitHub

- Microsoft Produkt
- nicht Open Source
- Hilft mit UI für das Management
- Static Hosting
- Communityaspekt


</Slide>


<Slide>

## GitLab

- semi Open Source (Open Source Freemium)
- Hilft mit UI für das Management
- gute Projektmanagement Tools
- Issues, Epics, Milestone
- Static Hosting
- Premium Feature: Epics / Gantt Charts


</Slide>


<Slide center>

## Git im Einsatz

*Diese Slides / Page*

</Slide>

</SlideDeck>