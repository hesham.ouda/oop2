---
title: "Versuch 3: Plot Panel"
author: [Manuel Di Cerbo]
subject: "oop2"
sidebarDepth: 2
---

# Versuch 3: Plot Panel

In diesem Versuch wird mit einem Plot und Audiowiedergabe gearbeitet.

### Ziele

1. GridBagLayout anwenden
2. Arbeiten mit Arrays
3. Model View Controller anwenden
4. Selbständiges erstellen einer Applikationsstruktur

![Panel](./screen.png)

## Git Projekt

Klonen Sie das Repo `git@gitlab.fhnw.ch:oop/oop2-versuche/v03-waveform-player.git` in Ihren Eclipse Workspace und importieren Sie das Projekt.

## Aufgabe 1

Erstellen Sie eine Applikation, welche das `PlotPanel` instanziert und stellen Sie eine Sinusfunktion dar, wie im Screenshot gezeigt.

![Panel](./screen1.png)

- Verwenden Sie ein `GridBagLayout`, da das GUI später erweitert wird.
- Schauen Sie sich die Klasse `PlotPanel` an. Wie können Sie die Klasse einbinden und verwenden?
- `PlotPanel` soll mit min, max `Integer.MIN_VALUE` und `Integer.MAX_VALUE` initialisiert werden.
- Das `PlotPanel` nimmt ein Array von Werten entgegen und plottet diese mit der `update` Methode.
- Erstellen Sie hierzu eine Methode `int generateSin(double t, double freq)`, welche Werte zwischen `Integer.MIN_VALUE` und `Integer.MAX_VALUE` zurükgibt.
- Rufen Sie diese Methode in einem Loop auf, um ein Array mit Werten zu füllen.

## Aufgabe 2

![Panel](./screen.png)

Erweitern Sie das GUI gemäss dem Screenshot. Foglende Funktionalität soll erreicht werden:

1. Gehen Sie gemäss dem MVC Konzept vor.
1. Geplottet wird ein Sinus mit der Frequenz, welche mit dem Slider eingestellt wird.
1. Sobald der Slider verschoben wird, soll der Plot neu gerechnet werden.
1. Es sollen 44100 Samples gezeichnet werden. Dies entspricht einer Sekunde Audiosamples.
1. Das Array `samples` sowie die aktuelle `frequency` sollen im Model geführt werden.
1. Ändert die `frequency` im GUI, so soll das Model die Samples neu berechnen.
1. `Controller` ist das Bindeglied von Model und View. Dabei brauchen Sie keinen Observer. `Controller` soll `View` updaten, sobald `Model` neu gerechnet hat.
1. Bei hohen Frequenzen ist der Plot schwer lesbar. Ändern Sie die Parameter bei `PlotPanel` so, dass Sie nur ein paar hundert Samples anzeigen. Das Signal ist ja schliesslich periodisch.
1. Setzen Sie beim `PlotPanel` den Labeltext und geben Sie die Periode in `[ms]` an.

## Aufgabe 3

Der Play Button soll die geplotteten Samples via Audio abspielen.

1. Binden Sie AudioPlayerEngine im Model ein.
1. Verwenden Sie die `play` Methode, sobald der User _Play_ drückt.
