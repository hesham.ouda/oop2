---
title: "Versuch 0: Setup"
author: [Manuel Di Cerbo]
subject: "oop1"
sidebarDepth: 2
---

# Setup

[[TOC]]

Folgend eine Screen-By-Screen Anleitung um Eclipse auf Windows zu installieren.

Video zur Übung

 [![https://www.eclipse.org/](./video.png)](https://tube.switch.ch/videos/f41ce9ca)


## Eclipse herunterladen und installieren

1. [https://www.eclipse.org/](https://www.eclipse.org/) besuchen

![Install](./install01.png)

2. Download auswählen

![Install](./install02.png)

![Install](./install03.png)

3. Unter `Downloads` speichern und noch **nicht** starten.

4. [https://jdk.java.net/14/](https://jdk.java.net/14/) besuchen
5. Windows / ZIP auswählen 

![Install](./install04.png)

6. In Downloads (oder wo anders) extrahieren

::: danger Verzeichnis jetzt wählen

Das Verzeichnis, welches Sie zur Installation wählen kann später nur umständlich gewechselt werden. Falls Sie `Downloads` nicht bevorzugen, wählen Sie hier ein anderes Verzeichnis (z.B. das Home Verzeichnis Ihres Users).

:::

![Install](./install05.png)
![Install](./install06.png)


![Install](./install07.png)

7. Eclipse installer starten



![Install](./install08.png)

8. `javaw` von JDK auswählen.

![Install](./install09.png)
![Install](./install10.png)
![Install](./install11.png)
![Install](./install12.png)
![Install](./install13.png)

9. Neuen Workspace Folder für OOP1 auf dem Desktop erstellen. (Oder alternativ an einem Ort wo der Order einfach zugänglich ist)

![Install](./install14.png)
![Install](./install15.png)
![Install](./install16.png)
![Install](./install17.png)

10. zur Workbench wechseln

![Install](./install18.png)

11. Neues Java Projekt erstellen

![Install](./install19.png)
![Install](./install20.png)
![Install](./install21.png)

11. Neue Klase im Projekt erstellen

![Install](./install22.png)
![Install](./install23.png)

12. Main Funktion in der Klasse erstellen (nach `main` `CTRL-Space` für Autocomplete verwenden).

![Install](./install24.png)
![Install](./install25.png)
![Install](./install26.png)

13. Ausführen und verifzieren
    
![Install](./install27.png)
