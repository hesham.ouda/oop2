---
title: "Versuch 1: Intro"
author: [Manuel Di Cerbo]
subject: "oop2"
sidebarDepth: 2
---

# Versuch 1: Intro

Ziel dieser Übung ist es, den Umgang mit Klassen und Objekten zu üben. Hierzu soll ein Panel zur
Darstellung von Personen implementiert werden.

[[TOC]]

![Screenshot](./screen.png)

Verwenden Sie `git` und clonen Sie folgendes Projekt in Ihren Eclipse Workspace:

```
https://gitlab.fhnw.ch/oop/oop2-versuche/v01-intro
```

Dannach können Sie das Projekt wie gewohnt als Verzeichnis importieren.

## Personen Panel

Das PersonenPanel hat zu diesem Zwecke zwei (oder mehrere) Objekte der Klasse Person. Die Klasse
Person kapselt die zu einer Person zugehörigen Daten (Name und Vorname) sowie ein Bild und
einen AudioClip der Person. Die Klasse verfügt über eine Methode paintComponent(), mit der Text
und das Bild gezeichnet wird.

## Anforderungen

Nach dem Start sollen zwei Personen sichtbar sein
Drückt man die Maus auf einer Person, soll das Panel seine Farbe ändern und der zugehörige
AudioClip ertönen. Wird die Maus wieder losgelassen, so ändert sich die Farbe wieder
zurück.

## Helpers

```java
// Laden der Bilder
bild = Utility.loadResourceImage(bildDatei);

// Laden der AudioClips
audioClip = Utility.loadAudioClip(audioDatei);

// Abspielen der AudioClips
audioClip.setFramePosition(0);
audioClip.start();
```

## Aufgabe 1: Grundgerüst

Erstellen Sie das Grundgerüst anhand des Klassendiagramms. Die Klasse Person soll hierbei als
eigene Klassendatei organisiert werden.

![Klassendiagramm](./uml.png)

## Aufgabe 2: Implementierung Klasse Person


### Konstruktor

- initiiert Attribute und lädt Clip und Bild gemäss Übergabeparameter
- registriert Mouselistener

### paintComponent

- ruft paintComponent() der Superklasse auf
- setzt je nach Flag die Farbe (z.B. Color.lightGray und Color.pink) und zeichnet mit fillRect ein ausgefülltes Rechteck mit Koordinaten(5, 5, 205, 340) )
- der schwarze Rahmen soll dann bei (5, 5, 205, 340) mit drawRect gezeichnet werden
- die weiteren Koordinaten für die Namen und das Bild sind

```

Vorname: x=20, y= 20 (drawString)
Name: x=20, y= 40 (drawString)
bild: x=20, y= 50 (drawImage)

```

### mousePressed()

- hier wird das paintFlag gesetzt, der AudioClip abgespielt und ein Neuzeichnen
ausgelöst

### mouseReleased()

- paintFlag zurücksetzen und Neuzeichnen auslösen

## Aufgabe 3: Konstruktor der Klasse PersonenPanel

- Erzeugen Sie nun noch im Konstruktor die beiden Personen und fügen Sie die Panel hinzu.

```
person1: (15, 0, 215, 350)
person2: (230, 0, 215, 350)
```

## Aufgabe 4

Bonus: Ersetzen Sie die zwei Personen mit Mario und Link, sowie deren Theme


