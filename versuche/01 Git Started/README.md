---
title: "Git"
author: [Manuel Di Cerbo]
subject: "oop2"
sidebarDepth: 2
---

# Let's `git` stated

[[TOC]]

In diesem Versuch lernen wir `git` kennen.

Videoserie (erste 4 Videos <https://tube.switch.ch/channels/416a8963>):

1. [![Video](./video.png)](https://tube.switch.ch/videos/043c783a)
1. [🎬 Git - Clone and Merge](https://tube.switch.ch/videos/ade76a7a)
1. [🎬 GitLab - Erstes Repository, SSH Keys](https://tube.switch.ch/videos/7e99bed6)
1. [🎬 GitLab - Issues, Milestones, Epics](https://tube.switch.ch/videos/95e3c5fd)

# Aufgabe

- Folgen Sie dem Video, installieren Sie Git und clonen Sie das Repository.
- Erstellen Sie einen GitLab account auf https://gitlab.fhnw.ch/
- Erstellen Sie SSH Keys und hinterlegen Sie diese in GitLab\
- Erstellen Sie ein HelloWorld Projekt auf GitLab
- Clonen Sie das Projekt lokal
- Erstellen Sie ein neues File `README.md`
- Pushen Sie Ihre Änderung und verifizieren Sie auf GitLab, dass es geklappt hat.