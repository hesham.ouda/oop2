---
title: "Versuch 2: GridBagLayout"
author: [Manuel Di Cerbo]
subject: "oop2"
sidebarDepth: 2
---

# Versuch 2: GridBagLayout

## Aufgabe 1

Ändern Sie Versuch 1 so ab, dass Sie die zwei Charaktere via `GridBagLayout` anzeigen.

- Die zei `PersonenPanel` sollen neu mit dem Fenster zusammen wachsen und sich den verbleibenden Platz fair aufteilen.
- Verwenden Sie als Basisklasse pro Charakter ein `JPanel`
- Ordnen Sie innerhalb des `JPanels` das `JLabel` mit dem Namen und ein `JLabel` mit dem Bild vertikal via `GridBagLayout` an.

![Panel](./screen.png)
