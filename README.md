---
title: OOP2
sidebarDepth: 3
footer: © Manuel Di Cerbo FHNW
sidebar: false
---

# Objektorientierte Programmierung 2

<img src="./logo.svg" width="100">

## Inhalt

- [Slides](./slides/01%20Git)
- [Versuche](./versuche/00%20Setup)
- [Drehbuch](./drehbuch/)

## Cheatsheet

<a href="/oop1/cheatsheet.pdf" style="width: 250px"><img src="./cheatsheet.png" alt="Cheatsheet"></a>

## Lektionen

<LectureTable />